<?php
/**
 * Created by PhpStorm.
 * User: gwl
 * Date: 16/9/18
 * Time: 上午9:07
 */

namespace app\common\model;


use think\Model;

class TestLog extends Model
{
    protected $table = 'jht_test_log';
    protected $pk    = 'id';

    protected $insert = ['createdate'];
   // protected $update = ['updatetime'];

    function setCreatdataAttr(){
        return date('Y-m-d H:i:s',time());
    }


}