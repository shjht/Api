<?php
/**
 * Created by PhpStorm.
 * User: gwl
 * Date: 16/9/18
 * Time: 上午9:06
 */

namespace app\common\model;


use think\Model;

class TopicLog extends Model
{
    protected $table = 'jht_topic_log';
    protected $pk    = 'id';

}