<?php

namespace app\common\model;

use think\Model;

class Option extends Model
{
    //
    protected $table = 'jht_option';
    protected $pk    = 'id';
}
