<?php

namespace app\common\model;

use think\Model;

class Question extends Model
{
    protected $table = 'jht_question';
    protected $pk    = 'id';




    public function options(){
        return $this->hasMany('Option','qsort');
    }
}
