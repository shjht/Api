<?php

namespace app\common\model;

use think\Model;

class Sms extends Model
{
   protected $table = 'cmf_plugin_sms_log';
   
   //获取最新验证码
   public static function getSms($mobile)
   {
   		$sms = new Sms();
   		$data = $sms->where('mobile','=',$mobile)->where('expiretime','>',time())->limit(1)->order('id','desc')->find();
   		if(empty($data))
   		{
   			return false;
   		}else{
   			$data = $data->data;
   			return $data['captcha'];
   		}
   	
   }
}
