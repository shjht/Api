<?php

namespace app\common\model;

use think\Model;

class Topic extends Model
{
    protected $table = 'jht_topic';
    protected $pk    = 'id';

}
