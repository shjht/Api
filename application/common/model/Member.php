<?php
namespace app\common\model;

use think\Db;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/9
 * Time: 18:41
 */

class Member extends \think\Model
{
    protected $table = 'jht_member';
    protected $pk    = 'id';

    protected $insert = ['createtime','updatetime','uid'];
    protected $update = ['updatetime'];

    function setCreatetimeAttr(){
        return date('Y-m-d H:i:s',time());
    }

    function setUpdatetimeAttr(){
        return date('Y-m-d H:i:s',time());
    }

    function setUidAttr(){
        return $this->getuid();
    }

    public function profileEdit(array $data,$id){
       $update_data = [
           'username' => $data['username'],
           'sex'    => $data['sex'],
           'idtype' => $data['idtype'],
           'wechat' => $data['wechat'],
           'email'  => $data['email'],
           'idnumber' => $data['idnumber'],
           'address' => $data['address'],
           'htqs'   => $data['htqs'],
           'referer' => $data['referer'],
       ];
       return $this->where('id = '.$id)->update($update_data);
       //return $this->userAddField()->update($update_date,['id'=>$id]);
    }

    public function userAddField()
    {
        return $this->setField(['username', 'email', 'sex', 'mobile', 'htqs', 'idnumber', 'idtype','sex','address']);
    }

    public function getuid(){
        $Member = new Member();
        $topid = $Member->max('uid')?$Member->max('uid'):80001001;
        $topid1 =  substr((string)$topid,0,4);
        $baseid = substr((string)$topid,4,5);

        if ($baseid == 9999){
            $topid1++;
            $baseid ='0001';
        }else{
            $baseid++;
        }
        return intval($topid1.$baseid);
    }

    /*
    protected $field = [
        'id' => 'int',
        'name', 'email',
    ];
    */
  //判断用户是否存在
  public static function isExists($mobile,$username )
  {
  	$result = Db::table('jht_member')->where(['mobile'=>$mobile,'username'=>$username])->select();
  	if($result)
  	{
  		return true;
  	}else{
  		return false;
  	}
  }
  //判断手机号是否注册
  public static function isMobile($mobile)
  {
  	$result = Db::table('jht_member')->where(['mobile'=>$mobile])->select();
  	if($result)
  	{
  		return true;
  	}else{
  		return false;
  	}
  }
  
  //获取全部信息
  public static function allInfo($mobile)
  {
  	$result = Db::table('jht_member')->where(['mobile'=>$mobile])->find();
  	return $result;
  }
  

}