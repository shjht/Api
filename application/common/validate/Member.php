<?php
namespace app\common\validate;

use think\Validate;

class Member extends Validate
{
    protected $rule = [
        'username' => 'require|max:30|min:2|checkName:thinkphp',
      //  'email' => 'email|checkMail:thinkphp',
       // 'idnumber' => 'require|number|max:30|min:8',
        'mobile' => 'require|checkMobile:thinkphp',
        //'address' => 'require',
        //'idtype' => 'number',
    ];

    protected $message =  [
        'mobile.require' => '手机号码必须',
        'username.require' => '名称必须',
        'name.max'     => '不能超过30个字符',
        'idtype.number'   => '证件类型数字',
        'email'        => '邮箱格式错误',
        'email.editMail' => '邮箱已被注册',
        'username.checkName' => '数据库存在该用户',
        'email.checkMail' => '数据库存在该邮箱',
        'idnumber.checkIdnumber' => '数据库存在证件号',
        'mobile.checkMobile' => '存在该手机号',
        'mobile.regxMobile' => '手机号格式错误',
    ];


    //场景
    protected $scene=[
        'edit' => ['username','email'=>'require|editMail|email','idtype','idnumber'],
        'add'  => ['username','mobile'],
    ];


    protected function editMail($vlaue,$ruel,$data){
        $id = $data['id'];
        $Model = new \app\common\model\Member();
        $info = $Model->where('id !='.$id)->where("email",$vlaue)->count();
        if ($info){
            return false;
        }else{
            return true;
        }
    }



        protected function regxMobile($vlaue,$ruel,$data){
        if(preg_match('/\(?\d{3}\)?[-\s.]?\d{3}[-\s.]\d{4}/x', $vlaue)) {
            return true;
        } else {
            return false;
        }

    }
    protected function checkMobile($value,$rule,$data){
        $Model = new \app\common\model\Member();
        $info = $Model->where("mobile",$value)->count();
        if ($info){
            return false;
        }else{
            return true;
        }
    }



    protected function checkName($value,$rule,$data){
        $id = $data['id'];
        $Model = new \app\common\model\Member();
        $info = $Model->where('id !='.$id)->where("username",$value)->count();
        if ($info){
            return false;
        }else{
            return true;
        }

    }

    protected function checkMail($value,$rule,$data){
        $Model = new \app\common\model\Member();
        $info = $Model->where("email",$value)->count();
        if ($info){
            return false;
        }else{
            return true;
        }
    }


    protected function checkIdnumber($value,$rule,$data){
        $Model = new \app\common\model\Member();
        $info = $Model->where("idnumber",$value)->count();
        if ($info){
            return false;
        }else{
            return true;
        }
    }
}