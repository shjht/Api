<?php

namespace app\common\controller;

use think\Controller;
use think\Response;

class UtilController extends Controller
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //json格式返回
    public static function jsonResponse($data,$type = 'json',$code = 200)
    {
    	return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
    
    //验证手机号格式
    public static function isMobile($mobile)
    {
    	return preg_match("/^1[34578][0-9]{9}$/", $mobile);
    }

    public static function getSignature($str, $key) {

        $signature = "";
        if (function_exists('hash_hmac')) {
            $signature = base64_encode(hash_hmac("sha1", $str, $key, true));
        } else {
            $blocksize = 64;
            $hashfunc = 'sha1';
            if (strlen($key) > $blocksize) {
                $key = pack('H*', $hashfunc($key));
            }
            $key = str_pad($key, $blocksize, chr(0x00));
            $ipad = str_repeat(chr(0x36), $blocksize);
            $opad = str_repeat(chr(0x5c), $blocksize);
            $hmac = pack(
                'H*', $hashfunc(
                    ($key ^ $opad) . pack(
                        'H*', $hashfunc(
                            ($key ^ $ipad) . $str
                        )
                    )
                )
            );
            $signature = base64_encode($hmac);
        }
        return $signature;
    }

    public static function levelToinfo($level){

        $level_data = [
            'level' => $level,
            'type' => '无',
            'bear' => '无',
            'product' => '无'
        ];
       switch ($level){
           case 1:
               $level_data['type']  = '保守型';
               $level_data['bear']  = '极低';
               $level_data['product'] = '低风险';
               break;
           case 2:
               $level_data['type']  = '稳健性';
               $level_data['bear']  = '较低';
               $level_data['product'] = '中低风险';
               break;
           case 3:
               $level_data['type']  = '平衡性';
               $level_data['bear']  = '一般';
               $level_data['product'] = '中风险';
               break;
           case 4:
               $level_data['type']  = '成长性';
               $level_data['bear']  = '较高';
               $level_data['product'] = '中高风险';
               break;
           case 5:
               $level_data['type']  = '进取型';
               $level_data['bear']  = '极高';
               $level_data['product'] = '高风险';
               break;
       }

        return $level_data;

    }
    
    
    //curl
    public function GetUrl($url)
    {
    	$ch = curl_init($url);
    	$timeout = 5;
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	$file_contents = curl_exec($ch);
    	curl_close($ch);
    	return $file_contents;
    }
    
    //验证手机号
    public static function checkMobile($mobile)
    {
    	if(empty($mobile))
    	{
    		$Success = false;
    		$Msg = "手机号不能为空!";
    		return self::jsonResponse(['Success'=>$Success,'Msg'=>$Msg]);
    	}
    	if(!self::isMobile($mobile))
    	{
    		$Success = false;
    		$Msg = "手机号码格式错误!";
    		return self::jsonResponse(['Success'=>$Success,'Msg'=>$Msg]);
    	}
    }
    
    //验证公爵号（1）5位以上  只能数字和英文  下划线
    public static function isDuke($duke)
    {
    	return preg_match("/^[a-zA-Z\d_]{5,}$/", $duke);
    }
    

   
}
