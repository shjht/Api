<?php

namespace app\api\model;

use think\Model;

class Relationship extends Model
{
    protected $table = 'cmf_term_relationships';
}
