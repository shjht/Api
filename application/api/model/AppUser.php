<?php

namespace app\api\model;

use think\Model;

class AppUser extends Model
{
    protected $table = 'app_user';
}
