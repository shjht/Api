<?php

namespace app\api\model;

use think\Model;

class Post extends Model
{
    protected $table = 'cmf_posts';
}
