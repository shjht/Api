<?php
use think\Route;
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // 定义资源路由
    '__rest__'=>[
       'Index'=>'api/index',
       'Nav'=>'api/nav',
       'Slide'=>'api/slide',
       'Links'=>'api/links',
       'Posts'=>'api/posts',
       'Test' => 'api/test',
    	'Menu'=>'api/menu',
    	'Carousel'=>'api/carousel',
    	'Detail'=>'api/detail',
    	'Wdetail'=>'api/wdetail',
       ':controller' => 'api/:controller',
    ],
];
