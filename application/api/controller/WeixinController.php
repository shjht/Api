<?php

namespace app\api\controller;

use think\Controller;
use think\Request;
use app\common\controller\UtilController;
use app\api\model\Weixin;
use app\common\model\Member;

class WeixinController extends Controller
{
	/*示例:
	 * https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxc212d3893556fbaa&redirect_uri=http%3a%2f%2fapi.ch-jht.com%2fapi%2fweixin&response_type=code&scope=snsapi_userinfo&state=chen#wechat_redirect
	 */
    //
    public function index()
    {
    	$uid = Request::instance()->get('state');
    	$info = array(
    			'appid'=>'wxc212d3893556fbaa',
    			'appsecret'=>'b8f4d72fc41074c06221cf48f7ac8987'
    	);
    	//回调地址
    	$redirect_url = urlencode('http://api.ch-jht.com/api/weixin');
    	//获取用户code 
    	$code = Request::instance()->get('code');
    	//code 如果为空 则调用授权页面 回调此函数
    	if(empty($code))
    	{
    		$this->oauth($info['appid'], $redirect_url);
    	}
    	//获取access_token 
    	$data = $this->access_token($info['appid'], $info['appsecret'], $code);
		if($data['status'])
		{
			$openid = $data['openid'];
			$member = new Member();
			$member->where('uid', $uid)->update(['openid' => $openid]);
			$this->redirect('http://m.ch-jht.com/question'.'?id='.$openid,302);
		}else{
			echo "系统错误，请稍后重试!";
		}
    }
    
    //微信网页授权   1
    public function oauth($appid,$redirect_url,$scope=true,$state='chen',$response_type='code')
    {
    	if($scope)
    	{
    		$scope = 'snsapi_userinfo'; //弹出网页授权页面
    	}else{
    		$scope = 'snsapi_base';		//静默授权
    	}
    	$map['appid']=$appid;
    	$map['redirect_uri']=$redirect_url;
    	$map['response_type']=$response_type;
    	$map['scope']=$scope;
    	$map['state']=$state;
    	$url = 'https://open.weixin.qq.com/connect/oauth2/authorize?'.http_build_query($map).'#wechat_redirect';
    	redirect($url);
    }
    
    //获取access_token  2
    public function access_token($appid,$secret,$code,$grant_type='authorization_code')
    {
    	$map['appid']=$appid;
    	$map['secret']=$secret;
    	$map['code']=$code;
    	$map['grant_type']=$grant_type;
    	$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?'.http_build_query($map);
    	$content = (new UtilController())->GetUrl($url);
    	$content = json_decode($content,true);
    	if(!empty($content['errmsg']))
    	{
    		exit ($content['errcode'].'--'.$content['errmsg']);
    	}
    	//获取用户信息
    	if($content['scope'] == 'snsapi_userinfo')
    	{
    		$content = $this->oauth_userinfo($content['access_token'], $content['openid']);
    	}
    	return $content;
    }
    
    //刷新userinfo
    public function oauth_userinfo($access_token,$openid,$lang='zh_CN')
    {
    	$map['access_token']=$access_token;
    	$map['openid']=$openid;
    	$map['lang']=$lang;
    	$url = 'https://api.weixin.qq.com/sns/userinfo?'.http_build_query($map);
    	$content = (new UtilController())->GetUrl($url);
    	$content = json_decode($content,true);
    	$weixin = new Weixin();
    	$result = $weixin::where(['openid'=>$content['openid']])->find();
    	if(!$result)
    	{
    		$data = array(
    				'openid'=>$content['openid'],
    				'nickname'=>$content['nickname'],
    				'sex'=>$content['sex'],
    				'province'=>$content['province'],
    				'city'=>$content['city'],
    				'country'=>$content['country'],
    				'headimgurl'=>$content['headimgurl'],
    				);
    		if($weixin->save($data))
    		{
    			$content['status']=true;
    		}else{
    			$content['status']=false;
    		}
    		
    	}
    	
    	if(!empty($content['errmsg']))
    	{
    		exit ($content['errcode'].'--'.$content['errmsg']);
    	}
    	//已经存入数据的 设置true
    	$content['status']=true;
    	return $content;
    }
}
