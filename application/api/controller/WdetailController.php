<?php

namespace app\api\controller;

use think\Controller;
use think\Request;
use app\api\model\Relationship;
use think\Db;
use think\Response;

class WdetailController extends Controller
{
	public $Success = TRUE;
	public $Msg = "";
	public $Data = "";
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
    	
    	//投资者id号
        $term_id = 12;       
        //每页显示数目
        $num = 5;
        //page默认为0  然后请求第几页 就显示第几页
        $page = Request::instance()->get('page')?Request::instance()->get('page'):0;
		$objectid = Relationship::where('term_id',$term_id)->column('object_id');
		//$page第几页 $num显示几个数据
		$posts = Db::table('cmf_posts')->where('id','in',$objectid)->order('post_modified','desc')->page($page,$num)->select();
		if($posts)
		{
			foreach ($posts as $key=>$value)
			{
				$value['smeta'] = explode('"',$value['smeta']);
				$posts[$key]['smeta'] = 'http://www.ch-jht.com/data/upload/'.$value['smeta'][3];
			}
			if($posts = Db::table('cmf_posts')->where('id','in',$objectid)->order('post_modified','desc')->page($page+1,$num)->select())
			{
				$this->Success = true;
			}else{
				$this->Success = false;
			}
			$this->Msg = "投资者教育详细页";
			return $this->response((['Success'=>$this->Success,'Msg'=>$this->Msg,'Page'=>$page,'Data'=>$posts]));
		}else{
			$this->Success = false;
			$this->Msg = "没有数据";
		}		
		return $this->response(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$posts]);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    //获取详细内容
    public function read($id)
    {
    	$id = Request::instance()->get('id'); 
    	$data = Db::table('cmf_posts')->where('id',$id)->find();
    	if($data)
    	{
    		$this->Success = true;
    		$this->Msg = "文章详细内容";
    		return $this->response(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
    	}else{
    		$this->Success = false;
    		$this->Msg = "获取内容失败";
    		return $this->response(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
    	}
		
    }
    
    protected function response($data, $type = 'json', $code = 200)
    {
    	return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
    

}
