<?php
namespace app\api\controller;
use think\Controller;
use think\Request;
use think\Response;
use app\common\controller\CaptchaController;

class IndexController extends Controller
{
	public $Success = TRUE;
	public $Msg = "";
	public $Data = "";
	/**
	 * 显示资源列表
	 *
	 * @return \think\Response
	 */
	public function index()
	{
		$mobile = Request::instance()->get('mobile');
		$captcha = new CaptchaController();
		$captcha = $captcha->getCaptcha($mobile);
		$this->Success = true;
		$this->Msg = "获取成功!";
		
		$info = array(
            'name'=>'陈亮',
            'age'=>'25'
		);
		$data = array(
			"Success"=>$this->Success,
			'Msg'=>$this->Msg,
			'Data'=>$info
		);
		$type = 'json';
		$code = 200;
// 	   return json(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
		return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
	}

	
	/**
	 * 显示创建资源表单页.
	 *
	 * @return \think\Response
	 */
	public function create()
	{
		echo 1111;
	}

	/**
	 * 保存新建的资源
	 *
	 * @param  \think\Request  $request
	 * @return \think\Response
	 */
	public function save(Request $request)
	{
		//
	}

	/**
	 * 显示指定的资源
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function read($id)
	{
		return $id;
	}

	/**
	 * 显示编辑资源表单页.
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function edit($id)
	{
		echo 3333;
	}

	/**
	 * 保存更新的资源
	 *
	 * @param  \think\Request  $request
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * 删除指定资源
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function delete($id)
	{
		//
	}
}
