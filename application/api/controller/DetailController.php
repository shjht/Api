<?php

namespace app\api\controller;

use think\Controller;
use think\Response;
use app\api\model\Nav;
use app\api\model\Terms;
use app\api\model\Relationship;
use think\Db;

class DetailController extends Controller
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        echo "首页信息";exit;
    }

    public function read($id)
    {
        $nav_name = Nav::get(['id'=>$id])->getData('label');	//获取栏目名称     

		$term_id = Terms::get(['name'=>$nav_name])->getData('term_id');	//term_id
        
		//status = 1 没有进入回收站
		$objects = Relationship::where('term_id',$term_id)->where('status',1)->limit(10)->order('object_id','desc')->column('object_id');	//object_id与文章是绑定的
	
		if(!empty($objects))
		{
			$now = date('Y-m-d H:i:s',time());
			$posts = Db::table('cmf_posts')->where('id','in',$objects)->where('post_modified','lt',$now)->order('post_modified','desc')->select();				
			$this->Success = true;
			$this->Msg = "栏目文章列表";
			return $this->response((['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$posts]));
		}else{
			
			$this->Success = false;
			$this->Msg = "该栏目没有文章!";
			return $this->response((['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]));
			
		}
		
    }
    
 	protected function response($data, $type = 'json', $code = 200)
    {
        return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
    
    


}
