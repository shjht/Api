<?php

namespace app\api\controller;

use app\common\Model\Member;
use think\Cache;
use think\Controller;
use think\Request;
use app\common\model\Sms;
use app\common\controller\UtilController;

class AuthController extends Controller
{
    public $Success = true;

    public $Msg = "";
    public $Data = "";

    public function index()
    {
    	$code = Request::instance()->get('code');
    	$util = new UtilController();
    	$content = $util->GetUrl('https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxc212d3893556fbaa&secret=b8f4d72fc41074c06221cf48f7ac8987&code='.$code.'&grant_type=authorization_code');
    	$content = json_decode($content);
    	$member = new \app\common\model\Member();
    	$results = $member->where(['openid'=>$content->openid])->find();
    	if(empty($results))
    	{
    		return redirect('http://m.ch-jht.com/question/');
    	}else{
    		//绑定成功之后跳转的页面
    		var_dump($results);exit;
    	}
    }

    public function register()
    {
    	$mobile = Request::instance()->get('mobile')?Request::instance()->get('mobile'):1;
    	$username = Request::instance()->get('username')?Request::instance()->get('username'):1;
    	$captcha = Request::instance()->get('captcha')?Request::instance()->get('captcha'):1;
    	if($mobile == 1 || $username == 1 || $captcha == 1)
    	{
    		$this->Success = false;
    		$this->Msg = "信息不能为空";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	
    	if(!UtilController::isMobile($mobile))
    	{
    		$this->Success = false;
    		$this->Msg = '手机格式错误!';
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	//获取最新验证码
    	$code = Sms::getSms($mobile);
    	if(!$code)
    	{
    		$this->Success = false;
    		$this->Msg = "验证码错误,请重新获取验证码!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($code != $captcha)
    	{
    		$this->Success = false;
    		$this->Msg = "验证码错误,请重新获取验证码!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	//判断是否存在
    	$result = \app\common\model\Member::isExists($mobile,$username);
    	if($result)
    	{
    		//该用户已经存在  TODO
    		$this->Success = true;
    		$this->Msg = "用户已注册！";
    		$info = Member::allInfo($mobile);
    		$arr = array(
    				'uid'=>$info['id']
    		);
    		//生成token返回
            $token = uniqid();
    		//$token = $this->getSignature($username, time());
    		//把token存入redis缓存
    		Cache::set($token, $arr,3600);
    		$data['access_token'] = $token;
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
    	}else{
    		//判断手机号是否已经注册
    		if(Member::isMobile($mobile))
    		{
    			$this->Success = false;
    			$this->Msg = "姓名与手机号码不匹配!";
    			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    		}
    		//获取uid
    		$get = new \app\common\model\Member();
    		//用户不存在 需要存入数据库
    		$data = array(
    				'username'=>$username,
    				'mobile'=>$mobile,
    				'uid'=>$get->setUidAttr()
    		);
    		if((new \app\common\model\Member())->save($data))
    		{
    			//存入成功之后跳转  TODO
    			$this->Success = true;
    			$this->Msg = "注册成功!";
    			$info = \app\common\model\Member::allInfo($mobile);
    			//生成token
                $token = uniqid();

                //$token = $this->getSignature($username, time());
    			$arr = array(
    					'uid'=>$info['id']
    			); 		
    			//把token存入redis缓存
    			Cache::set($token, $arr,3600);
    			$data['access_token'] = $token;
    			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
    		}
    	}
    }

    function getSignature($str, $key)
    {
    	$signature = "";
    	if (function_exists('hash_hmac'))
    	{
    		$signature = base64_encode(hash_hmac("sha1", $str, $key, true));
    	} else {
    		$blocksize = 64;
    		$hashfunc = 'sha1';
    		if (strlen($key) > $blocksize)
    		{
    			$key = pack('H*', $hashfunc($key));
    		}
    		$key = str_pad($key, $blocksize, chr(0x00));
    		$ipad = str_repeat(chr(0x36), $blocksize);
    		$opad = str_repeat(chr(0x5c), $blocksize);
    		$hmac = pack(
    				'H*', $hashfunc(
    						($key ^ $opad) . pack(
    								'H*', $hashfunc(
    										($key ^ $ipad) . $str
    										)
    								)
    						)
    				);
    		$signature = base64_encode($hmac);
    	}
    	return $signature;
    }
    
    
 }

