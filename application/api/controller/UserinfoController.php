<?php

namespace app\api\controller;

use app\common\controller\UtilController;
use app\common\model\Member;
use think\Controller;
use think\Request;
use think\Cache;
use think\Db;
use think\Response;
use think\Validate;
class UserinfoController extends Controller
{

    function save(){
        $request = Request::instance();
        if ($request->isPost()){
            $data = Request::instance()->post();
            $Member = new Member();
            $MemberValidate = new \app\common\validate\Member();
            $token = $data['token'];
            $token = Cache::get($token);
            if(!$token){
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>'请重新登录']);
            }
            $_POST['id'] = $token['uid'];
            if( $MemberValidate->scene('edit')->check($_POST) === false){
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>$MemberValidate->getError()]);
            }
            $_POST['id'] = $token['uid'];
            $info = $Member->profileEdit($_POST,$token['uid']);
            if (($info === false)) {
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>'修改失败']);
            }else{
                $record = $this->Testrecord($token['uid']);
                if($topic_id = $this->getTopic()) return $this->response(['Success'=>true,'Msg'=>'修改成功','Data'=> ['topic'=>$topic_id,'record'=>$record]]);
            }
        }
    }

    function info(){
        $request = Request::instance();
        if ($request->isPost())
        {

            $Member = new Member();
            $MemberValidate = new \app\common\validate\Member();
            $token = Cache::get($_POST('token'));
            if(!$token){
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>'请重新登录']);
            }
            $_POST['id'] = $token['uid'];
            if( $MemberValidate->scene('edit')->check($_POST) === false){
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>$MemberValidate->getError()]);
            }
            $_POST['id'] = $token['uid'];
            $info = $Member->profileEdit($_POST,$token['uid']);
            if (($info === false)) {
                return $this->response(['Success'=>false,'Msg'=>'错误','Data'=>'修改失败']);
            }else{
                if($topic_id = $this->getTopic()) return $this->response(['Success'=>false,'Msg'=>'修改成功','Data'=> ['topic'=>$topic_id]]);
            }

        }else{
            $token = Cache::get(Request::instance()->get('token'));
            if ($token){
                $Member = new Member();
                $info = $Member->get($token['uid'])->toArray();
                return $this->response(['Success'=>true,'Msg'=>'获取成功','Data'=>$info]);
            }else{
                return $this->response(['Success'=>false,'Msg'=>'认证失败','Data'=>[]]);
            }
        }
    }


    //是否评测过
    protected function Testrecord($uid){
      //  $uid = Request::instance()->get('uid');
        $Member = new Member();
        $userinfo = $Member->get($uid)->toArray();
        if ($userinfo['level'] == 0){
            return false;
        }else{
            return true;
        }

    }

    //获取评测结果
    public function getRecord(){
        $token = Cache::get(Request::instance()->get('token'));
        if ($token){
            $Member = new Member();
            $user = $Member->get($token['uid'])->toArray();
            $level_info = UtilController::levelToinfo($user['level']);
            return $this->response(['Success'=>true,'Msg'=>'获取成功','Data'=>$level_info]);
        }
    }



    protected function getTopic(){
        $topic = Db::table('jht_topic')->where('status=1')->order('createdate','desc')->field('id')->find();
        if ($topic){
            return $topic['id'];
        }else{
            return false;
        }
    }

    function condition(){
        $request = Request::instance();
        if ($request->isPost()){
            $token = $request->param('token');
            if($token = Cache::get($token)){
                $member = new Member();
                $asset = $request->param('asset');
                $revenue = $request->param('revenue');
                if($asset==0 && $revenue==0) return $this->response(['Success'=>false,'Msg'=>'必须选择一项','Data'=>[]]);
                $res = $member->update([
                    'asset' => $asset,
                    'revenue' => $revenue,
                ],['id'=>$token['uid']]);
                if ($res)  return $this->response(['Success'=>true,'Msg'=>'资产收入确认成功','Data'=>[]]);
                else return $this->response(['Success'=>false,'Msg'=>'资产收入确认失败','Data'=>[]]);
            }else{
                return $this->response(['Success'=>false,'Msg'=>'认证失败','Data'=>[]]);
            }

        }
    }


    function login(){
        Cache::set('token',['uid'=>18,'topic'=>16]);
    }

    protected function response($data, $type = 'json', $code = 200)
    {
        return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*','Access-Control-Allow-Methods'=>'POST'])->code($code);
    }
    
    //传递uid字段
    
    public function uid()
    {
    	$uid = Request::instance()->get('uid');
    	return redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxc212d3893556fbaa&redirect_uri=http%3a%2f%2fapi.ch-jht.com%2fapi%2fweixin&response_type=code&scope=snsapi_userinfo&state='.$uid.'#wechat_redirect');
    }
}
