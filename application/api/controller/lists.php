<?php

namespace app\api\controller;

use think\Controller;
use think\Request;

class lists extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->response('b');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {

        $field = !empty($tag['field']) ? $tag['field'] : '*';
        $limit = !empty($tag['limit']) ? $tag['limit'] : '';
        $order = !empty($tag['order']) ? $tag['order'] : 'post_date';

        //根据参数生成查询条件
        $where['status'] = array('eq',1);
        $where['post_status'] = array('eq',1);
        if (!empty($tag['cid'])) {
            $where['term_id'] = array('in',$tag['cid']);
        }
        if (!empty($tag['ids'])) {
            $where['object_id'] = array('in',$tag['ids']);
        }
        if (!empty($tag['where'])) {
            $where['_string'] = $tag['where'];
        }

        $join = "".C('DB_PREFIX').'posts as b on a.object_id =b.id';
        $join2= "".C('DB_PREFIX').'users as c on b.post_author = c.id';
        $join3= "left join ".C('DB_PREFIX').'attachment as t on b.id = t.host_id';
        $rs= Db::table('cmf_term_relationships');

    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
