<?php

namespace app\api\controller;

use think\Controller;
use app\common\controller\CaptchaController;
use think\Request;
use think\Response;

class MobileController extends Controller
{
	protected $Success = true;
	protected $Msg = "";
    //接口文档调用 
    
	//获取验证码
    public function index()
    {
    	$mobile = Request::instance()->get('mobile');
    	$captcha = new CaptchaController();
    	return  $captcha->getCaptcha($mobile);

    }
    
    //返回类型
    protected function response($data, $type = 'json', $code = 200)
    {
    	return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
}
