<?php

namespace app\controller;

use think\Config;
use think\Controller;
use think\controller\Rest;
use think\Request;
use think\Db;
use app\model\posts as postsModel;
class posts extends Rest
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        var_dump($_GET);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $field = "object_id,post_keywords,post_source,post_date,post_content,post_modified,user_nicename";
        $where['status'] = array('eq',1);
        $where['tid'] = array('eq',$id);

        $model = Db::table('cmf_term_relationships');
        $post = $model
            ->alias('a')->join(Config::get('database.prefix').'posts b','a.object_id =b.id')
            ->join(Config::get('database.prefix').'users c','b.post_author = c.id')
            ->field($field)
            ->where($where)
            ->select();

        return $this->response($post);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
