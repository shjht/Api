<?php

namespace app\api\controller;

use think\Controller;
use think\Request;
use app\api\model\AppUser;
use think\Response;

class AppController extends Controller
{
    //
    protected $result;
    protected $data;
    //注册
    public function reg()
    {
    	//前端传递的是json格式
    		$content = Request::instance()->getContent();
    		$content = json_decode($content);
    		$usernmae = $content->username;
    		$passwd = $content->passwd;
    		$nickname = $content->nickname;
			$appuser = new AppUser([
			    'username'  =>  $usernmae,
			    'passwd' =>  $passwd,
				'nickname'=>$nickname
			]);
    		if($appuser->save())
    		{
    			return $this->response(['data'=>'注册成功','result'=>1]);
    		}else{
    			return $this->response(['data'=>'注册失败','result'=>2]);
    		}

    }
    //登录
    public function login()
    {
			$username = Request::instance()->post('username');
			$passwd = Request::instance()->post('passwd');
	    	$result = AppUser::where(['username'=>$username,'passwd'=>$passwd])->find();
			if($result)
			{
				return $this->response(['data'=>'登录成功','result'=>1]);
			}else{
				return $this->response(['data'=>'登录失败','result'=>2]);
			}
    }
    
    //新闻端列表
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    protected function response($data, $type = 'json', $code = 200)
    {
    	return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
}
