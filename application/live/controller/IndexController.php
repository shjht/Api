<?php
namespace app\live\controller;

use think\Controller;
use think\Request;
use think\Response;
use think\Db;

/**
* 直播邀请码界面
*/
class IndexController extends Controller
{
	public $table = 'live_code';
	public $table_visit = 'live_visit';

	public function index() {
		$code = Request::instance()->get('code');
		$code = trim($code);

		if ($code == "") {
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'授权码不能为空']);
		}

		$ip = Request::instance()->ip();
		$date = date('Y-m-d h:i:s');
		$conditions = ['code'=> $code];
		$result = Db::table($this->table)->where($conditions)->field('id')->find();
		if ($result == null) {
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'授权码错误']);
		}
		$data = ['cid'=>$result['id'], 'ip'=>$ip, 'visitTime'=>$date];

		// 启动事务
		Db::startTrans();
		try{
			// 记录访问日志
			Db::table($this->table_visit)->insert($data);
			// 更新授权码的访问次数
			Db::table($this->table)->where($conditions)->setInc('count');
		    // 提交事务
		    Db::commit();
			return $this->jsonResponse(['Success'=> true, 'Msg'=>'成功']);
		} catch (\Exception $e) {
		    // 回滚事务
		    Db::rollback();
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'服务器错误']);
		}
	}

	public function show() {
		$data = Db::table($this->table)->field('name, code, count')->order('count desc, id')->select();

		return $this->jsonResponse(['Success'=> true, 'Msg'=>'成功', 'Data'=>$data]);
	}

	public function insertCode() {
		// 生成授权码
		// 授权码长度
		$length = Request::instance()->get('length');
		$length = trim($length);
		// 授权码数量
		$num = Request::instance()->get('num');
		$num = trim($num);
		// 授权码密码
		$passwd = Request::instance()->get('passwd');
		$passwd = trim($passwd);
		// 授权码是否清空原有授权码
		$status = Request::instance()->get('status');

		if ($length == "" || $num == "" || $passwd == "") {
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'长度、个数和密码不能为空']);
		}

		if($passwd != '888888') {
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'密码错误']);
		}

		if ($status) {
			// 插入随机数并清空原有数据
			$sql = 'truncate table '.$this->table;
			Db::table($this->table)->execute($sql);
			$arr = $this->makeCode($length, $num);

			return $this->jsonResponse(['Success'=> true, 'Msg'=>'更新成功', 'Data'=>$arr]);
		}

		$count = Db::table($this->table)->count();

		if($count >= $num ) {
			return $this->jsonResponse(['Success'=> false, 'Msg'=>'已存在'.$count.'条记录']);
		}
		$num = $num - $count;
		// 插入随机数
		$arr = $this->makeCode($length, $num);

		return $this->jsonResponse(['Success'=> true, 'Msg'=>'更新成功', 'Data'=>$arr]);
	}

	public function updateCount() {
		Db::table($this->table)->where('id', '<', 100)->update(['count'=> 0]);
		$sql = 'truncate table '.$this->table_visit;
		Db::table($this->table_visit)->execute($sql);
		return $this->jsonResponse(['Success'=> true, 'Msg'=>'更新成功']);
	}

	// 生成随机数
	public function makeCode($length = 8, $ind = 15) {
	    if ($ind == 0) {
	    	$arr = Db::table($this->table)->field('code')->select();
	    	return $arr;
	    }

	    $min = pow(10 , ($length - 1));
	    $max = pow(10, $length) - 1;
	    $rand = rand($min, $max);
		Db::table($this->table)->insert(['code'=>$rand]);
	    return self::makeCode($length, $ind - 1);
	}

	public function jsonResponse($data,$type = 'json',$code = 200) {
    	return Response::create($data, $type,200,['Access-Control-Allow-Origin'=>'*'])->code($code);
    }
}

?>