<?php

namespace app\api2\controller;

use think\Controller;
use think\Request;
use think\Cache;
use app\common\model\Member;
use app\common\controller\UtilController;
use app\common\model\Sms;
use app\common\controller\CaptchaController;

class ChangeMobileController extends AuthController
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //验证手机
    public function checkMobile()
    {
    	$token = Request::instance()->get('access_token');
    	$mobile = Request::instance()->get('mobile')?Request::instance()->get('mobile'):0;
    	$passwd = Request::instance()->get('passwd');
    	if($mobile == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "请输入手机号";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(!UtilController::isMobile($mobile))
    	{
    		$this->Success = false;
    		$this->Msg = "手机号码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(empty($passwd))
    	{
    		$this->Success = false;
    		$this->Msg = "密码不能为空";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$id = Cache::get($token);
    	$info = Member::where(['id'=>$id])->find();
    	if($mobile != $info->mobile)
    	{
    		$this->Success = false;
    		$this->Msg = "原手机号错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(md5($passwd) != $info->passwd)
    	{
    		$this->Success = false;
    		$this->Msg = "密码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($mobile == $info->mobile && md5($passwd) == $info->passwd)
    	{
    		$this->Msg = "手机验证成功";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    		
    	}
    	
    }
    //保存新手机号-获取验证码
    public function getCaptcha()
    {
    	$newmobile = Request::instance()->get('newmobile');
    	if(empty($newmobile))
    	{
    		$this->Success = false;
    		$this->Msg = "手机号为空!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(!UtilController::isMobile($newmobile))
    	{
    		$this->Success = false;
    		$this->Msg = "手机格式错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$result = Member::where(['mobile'=>$newmobile])->find();
    	if($result)
    	{
    		$this->Success = false;
    		$this->Msg = "该手机号已注册!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$sms = new CaptchaController();
    	return $sms->getCaptcha($newmobile);
    }
    
   //保存新手机号-保存信息
   public function saveMobile()
   {
   	    $token = Request::instance()->get('access_token');
   		$newmobile = Request::instance()->get('newmobile');
   		$captcha = Request::instance()->get('captcha');
        $result = UtilController::checkMobile($newmobile);
        if($result)
        {
        	return $result;
        }
       $tmpdata = Member::where(['mobile'=>$newmobile])->find();
       if($tmpdata)
       {
       		$this->Success = false;
       		$this->Msg = "手机号已注册!";
       		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
       }
       if(empty($captcha))
       {
       		$this->Success = false;
       		$this->Msg = "验证码不能为空!";
       		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
       }
       $code = new Sms();
       $code = $code->getSms($newmobile);
       if($captcha != $code)
       {
       		$this->Success = false;
       		$this->Msg = "验证码错误!";
       		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
       }
       $id = Cache::get($token);
       $userinfo = new Member();
       $all = $userinfo->where('id', $id)->update(['mobile' => $newmobile]);
       if($all)
       {
       	  $this->Success = true;
       	  $this->Msg = "手机号变更成功!";
       	  return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
       }else{
       	  $this->Success = false;
       	  $this->Msg = "手机变更失败!";
       	  return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
       }
    	
   }
}
