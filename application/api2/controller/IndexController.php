<?php

namespace app\api2\controller;

use think\Controller;
use think\Request;
use think\Cache;
use app\common\model\Member;
use app\common\controller\UtilController;

class IndexController extends AuthController
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //个人主页信息
    public function index()
    {
    	$token = Request::instance()->get('access_token');
    	$id = Cache::get($token);
    	$info = Member::where(['id'=>$id])->find();
    	$data['username'] = $info->username;
    	return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
    }
   //个人信息
   public function userinfo()
   {
   		$tmpdata = array(1=>'身份证号',2=>'港澳台通行证',3=>'护照');
   		$token = Request::instance()->get('access_token');
   		$id = Cache::get($token);
   		$info = Member::where(['id'=>$id])->find()->toarray();
   		$data = array();
   		foreach ($info as $key=>$value)
   		{
   			$data[$key] = $value;
   		}
   		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$data]);
   }
   
   //保存个人修改的信息
   public function  saveUserinfo()
   {
   		$token = Request::instance()->get('access_token');
   		$id = Cache::get($token);
   		$username = Request::instance()->get('username');
   		$sex = Request::instance()->get('sex');
   		$idtype = Request::instance()->get('idtype');
   		$idcard = Request::instance()->get('idcard');
   		$wechat = Request::instance()->get('wechat');
   		$email = Request::instance()->get('email');
   		$address = Request::instance()->get('address');
   		$referer = Request::instance()->get('referer');
   		$htqs = Request::instance()->get('htqs');
   		$userinfo = new Member();
        $result = $userinfo->where('id', $id)->update([
        		'username' => $username,
        		'sex'=>$sex,
        		'idtype'=>$idtype,
        		'idnumber'=>$idcard,
        		'wechat'=>$wechat,
        		'email'=>$email,
        		'address'=>$address,
        		'referer'=>$referer,
        		'htqs'=>$htqs
        		]);
   		if($result)
   		{
   			$this->Msg = "信息更新成功!";
   			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
   		}else{
   			$this->Success = false;
   			$this->Msg = "信息更新失败!";
   			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
   		}
   		
   }
}
