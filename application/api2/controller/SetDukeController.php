<?php

namespace app\api2\controller;

use think\Controller;
use think\Request;
use think\Cache;
use app\common\controller\UtilController;
use app\common\model\Member;

class SetDukeController extends AuthController
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //设置公爵号-手机号验证
    public function checkMobile()
    {
    	$token = Request::instance()->get('access_token');
    	$mobile = Request::instance()->get('mobile');
    	$passwd = Request::instance()->get('passwd');
    	$result = UtilController::checkMobile($mobile);
    	if($result)
    	{
    		return $result;
    	}
    	if(empty($passwd))
    	{
    		$this->Success = false;
    		$this->Msg = "密码不能为空!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$id = Cache::get($token);
    	$userinfo = Member::where(['id'=>$id])->find();
    	if($mobile != $userinfo->mobile)
    	{
    		$this->Success = false;
    		$this->Msg = "手机号与登录号不匹配!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($userinfo->passwd!= md5($passwd))
    	{
    		$this->Success = false;
    		$this->Msg = "密码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$this->Msg = "验证成功!";
    	return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    }
    
    //设置公爵号  5位以上,只能数字和英文,下划线
    public function setDuke()
    {
    	$token = Request::instance()->get('access_token');
    	$id = Cache::get($token);
    	$duke = Request::instance()->get('duke');
    	$tmpdata = Member::where(['id'=>$id])->find();
    	if(!empty($tmpdata->duke))
    	{
    		$this->Success = false;
    		$this->Msg = "公爵号不能修改";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(empty($duke))
    	{
    		$this->Success = false;
    		$this->Msg = "公爵号不能为空!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$result = UtilController::isDuke($duke);
    	if($result)
    	{
    		$result = Member::where(['duke'=>$duke])->find();    //查找公爵号
    		if(empty($result))
    		{
    			$member = new Member();
    			$tmp = $member->where('id', $id)->update(['duke' => $duke]);
    			if($tmp)
    			{
    				$this->Msg = "公爵号设置成功!";
    				return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    			}
    		}else{
    			$this->Success = false;
    			$this->Msg = "该公爵号已存在!";
    			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    		}
    	}else{
    		$this->Success = false;
    		$this->Msg = "公爵号只能数字,英文字母,下划线开头 要求5位以上";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    }
}
