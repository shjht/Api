<?php

namespace app\api2\controller;

use think\Controller;
use think\Cache;
use app\common\controller\UtilController;
use think\Request;

class LogoutController extends AuthController
{
	//退出登录
	public function index()
	{
		$token = Request::instance()->get('access_token');
		$result = Cache::rm($token);
		if($result)
		{
			$this->Msg = "登出成功!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
		}
	}
}
