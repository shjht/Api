<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/24
 * Time: 16:43
 */

namespace app\api2\controller;


use app\common\controller\UtilController;
use think\Cache;
use think\Controller;
use think\Request;

class ApiBaseController extends Controller
{
    protected $Msg = '';
    protected $Data = [];
    protected $Success = false;
    public $Uid;
    public $Access_token;

    function _initialize()
    {
        $key =  Request::instance()->get('access_token');
        $this->Access_token = $key;
        $uid = Cache::get($key);
        if ($uid){
            $this->Uid = $uid;
        }else{
            $this->Msg = "身份验证失败";
            die($this->Msg);
        }
        parent::_initialize();
    }

    function call(){
       return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
    }

}