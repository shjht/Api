<?php

namespace app\api2\controller;

use think\Controller;
use think\Request;
use app\common\controller\UtilController;
use app\common\controller\CaptchaController;
use app\common\model\Sms;
use app\common\model\Member;

class PasswdController extends Controller
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //忘记密码-获取验证码
    public function getCaptcha()
    {
		$mobile = Request::instance()->get('mobile')?Request::instance()->get('mobile'):0;
		if($mobile == 0)
		{
			$this->Success = false;
			$this->Msg = "手机不能为空!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
		}
		if(!UtilController::isMobile($mobile))
		{
			$this->Success = false;
			$this->Msg = "手机号码错误!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
		}
		$getCaptcha = new CaptchaController();
		return $getCaptcha->getCaptcha($mobile);		
    }
    //忘记密码-提交表单
    public function updatePasswd()
    {
    	$mobile= Request::instance()->post('mobile')?Request::instance()->post('mobile'):0;
    	$captcha = Request::instance()->post('captcha')?Request::instance()->post('captcha'):0;
    	$newpasswd = Request::instance()->post('newpasswd')?Request::instance()->post('newpasswd'):0;
    	$renewpasswd = Request::instance()->post('renewpasswd')?Request::instance()->post('renewpasswd'):0;
    	if($captcha == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "验证码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($mobile == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "请输入手机号!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(!UtilController::isMobile($mobile))
    	{
    		$this->Success = false;
    		$this->Msg = "手机号码错误！";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	//验证验证码
    	$sms = new Sms();
    	if($captcha != $sms->getSms($mobile))
    	{
    		$this->Success = false;
    		$this->Msg = "验证码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($newpasswd == 0 || $renewpasswd == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "密码不能为空!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
    	}
    	if($newpasswd !== $renewpasswd)
    	{
    		$this->Success = false;
    		$this->Msg = "密码不匹配!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
    	}
		$user = Member::get(['mobile'=>$mobile]);
		$user->passwd = md5($newpasswd);
		if($user->save())
		{
			$this->Msg = "密码更新成功!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
		}else{
			$this->Success = false;
			$this->Msg = "密码更新失败!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
		}
    }
}
