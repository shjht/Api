<?php

namespace app\api2\controller;

use think\Controller;
use think\Request;
use app\common\controller\UtilController;
use app\common\controller\CaptchaController;
use app\common\model\Sms;
use app\common\model\Member;

class RegisterController extends Controller
{
	public $Success = true;
	public $Msg = "";
	public $Data = "";
    //发送验证码
    public function getCaptcha()
    {
    	$mobile= Request::instance()->get('mobile')?Request::instance()->get('mobile'):0;
    	//验证该手机号有没有注册
    	$member = new Member();
    	$result = $member->where('mobile', $mobile)->find();
		if(!empty($result))
		{
			$this->Success = false;
			$this->Msg = "该用户已注册!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
		}
    	if($mobile == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "请输入手机号!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(!UtilController::isMobile($mobile))
    	{
    		$this->Success = false;
    		$this->Msg = "手机号码错误!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$captch = new CaptchaController();
    	return $captch->getCaptcha($mobile);
    }  
    //保存用户注册信息
    public function saveUserinfo()
    {
    	$mobile= Request::instance()->post('mobile')?Request::instance()->post('mobile'):0;
    	$captcha = Request::instance()->post('captcha')?Request::instance()->post('captcha'):0;
    	$name = Request::instance()->post('name');
    	$referrer = Request::instance()->post('referrer');
    	$passwd = Request::instance()->post('passwd')?Request::instance()->post('passwd'):0;
    	$repasswd = Request::instance()->post('repasswd')?Request::instance()->post('repasswd'):0;
    	if($mobile == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "请输入手机号!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	//查询数据 防止恶意提交
        $member = new Member();
    	$result = $member->where('mobile', $mobile)->find();
		if(!empty($result))
		{
			$this->Success = false;
			$this->Msg = "该用户已注册!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
		}
		if(!UtilController::isMobile($mobile))
		{
			$this->Success = false;
			$this->Msg = "手机格式错误！";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
		}
		if($captcha == 0)
		{
			$this->Success = false;
			$this->Msg = "验证码错误!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
		}
		//验证验证码
		$sms = new Sms();
		if($captcha != $sms->getSms($mobile))
		{
			$this->Success = false;
			$this->Msg = "验证码错误!";
			return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
		}
		
    	if(empty($name))
    	{
    		$this->Success = false;
    		$this->Msg = "请输入用户名";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if(empty($referrer))
    	{
    		$this->Success = false;
    		$this->Msg = "请输入推荐人";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($passwd == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "密码不能为空";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($repasswd == 0)
    	{
    		$this->Success = false;
    		$this->Msg = "重复密码不能为空";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	if($passwd != $repasswd)
    	{
    		$this->Success = false;
    		$this->Msg = "密码不匹配";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg]);
    	}
    	$member = new Member();
    	$tmpdata = array(
    		'username'=>$name,
    		'referer'=>$referrer,
    		'mobile'=>$mobile,
    		'passwd'=>md5($passwd)
    	);
    	if($member->save($tmpdata))
    	{
    		$this->Msg = "注册成功!";
    		return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
    	}
    	return UtilController::jsonResponse(['Success'=>$this->Success,'Msg'=>$this->Msg,'Data'=>$this->Data]);
    }
}
